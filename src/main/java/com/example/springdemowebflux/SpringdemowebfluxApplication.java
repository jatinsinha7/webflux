package com.example.springdemowebflux;

import com.example.springdemowebflux.Repository.EmployeeRepository;
import com.example.springdemowebflux.model.Employee;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import reactor.core.publisher.Flux;

@SpringBootApplication
public class SpringdemowebfluxApplication {

    @Bean
    ApplicationRunner init(EmployeeRepository repository) {

        Object[][] data = {
                {"1", "Andrew", 300, "NDK"},
                {"2", "Andrew", 100, "Piranha"},
                {"3", "Andrew", 75, "Necky"}
        };

        return args -> {
            repository
                    .deleteAll()
                    .thenMany(
                            Flux.just(data)
                                    .map(array -> {
                                        return new Employee((String) array[0], (String) array[1], (Integer) array[2], (String) array[3]);
                                    })
                                    .flatMap(repository::save)
                    )
                    .thenMany(repository.findAll())
                    .subscribe(emp -> System.out.println("saving " + emp.toString()));
        };
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringdemowebfluxApplication.class, args);
    }

}
