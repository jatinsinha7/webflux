package com.example.springdemowebflux.model;


import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.annotation.Generated;

@Document(collection="Employee")
@Getter
@Setter
@ToString
@AllArgsConstructor
public class Employee {


    @Id
    private String id;
    private String name;
    private long salary;
    private String designation;

}
