package com.example.springdemowebflux.controller;

import com.example.springdemowebflux.Repository.EmployeeRepository;
import com.example.springdemowebflux.Service.WebClientService;
import com.example.springdemowebflux.model.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

@RestController
public class EmployeeController {

    private static Logger log = LoggerFactory.getLogger(EmployeeController.class);

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    WebClientService webClientService;

    @RequestMapping("/employee")
    public Flux<Employee> getAll()
    {
        return employeeRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.POST,value = "/employee")
    public Mono<Employee> add(@RequestBody Employee employee){
        System.out.println("saving " + employee.toString());
        return employeeRepository.save(employee);
    }
    @RequestMapping(method = RequestMethod.GET,value = "/employee/{id}")
    public Mono<Employee> add1(@PathVariable String id){
        //System.out.println("saving " + employee.toString());
        return employeeRepository.findById(id);
    }

    @RequestMapping(method = RequestMethod.PUT,value = "/employee/{id}")
    public Mono<Employee> change(@PathVariable String id,@RequestBody Employee employee){
        //System.out.println(id);
        return employeeRepository.save(new Employee(id,employee.getName(),employee.getSalary(),employee.getDesignation()));
    }

    @RequestMapping(method = RequestMethod.DELETE,value = "/employee/{id}")
    public Mono<Void> delete(@PathVariable String id){
        log.info("sadf");
        return employeeRepository.deleteById(id);
    }

    @RequestMapping("/employee/get/{id}")
    public Mono<Integer> get(@PathVariable String id){
        log.info("Started");
        Mono<Integer> monoEmployee= webClientService.getUser(id);
        return monoEmployee;
    }

}
