package com.example.springdemowebflux.Service;

import com.example.springdemowebflux.model.Employee;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class WebClientService {

    WebClient webClient = WebClient.create("http://localhost:8081");
    public Mono<Integer> getUser(String id) {
        return webClient
                .get()
                .uri("/all/{id}", id)
                .retrieve().bodyToMono(Integer.class).log();
    }
}
